bool CHARACTER::DoRefine2(LPITEM item, bool bMoneyOnly, LPITEM *refinePotions)
{
	if (!CanHandleItem(true))
	{
		ClearRefineMode();
		return false;
	}

	//°ł·® ˝Ă°ŁÁ¦ÇŃ : upgrade_refine_scroll.quest żˇĽ­ °ł·®ČÄ 5şĐŔĚł»żˇ ŔĎąÝ °ł·®Ŕ» 
	//ÁřÇŕÇŇĽö ľřŔ˝
	if (quest::CQuestManager::instance().GetEventFlag("update_refine_time") != 0)
	{
		if (get_global_time() < quest::CQuestManager::instance().GetEventFlag("update_refine_time") + (60 * 5))
		{
			sys_log(0, "can't refine %d %s", GetPlayerID(), GetName());
			return false;
		}
	}

	const TRefineTable * prt = CRefineManager::instance().GetRefineRecipe(item->GetRefineSet());

	if (!prt)
		return false;

	DWORD result_vnum = item->GetRefinedVnum();

	//LIL_TARD_BUGO
	DWORD result_fail_vnum = item->GetRefineFromVnum();

	// REFINE_COST
	int cost = ComputeRefineFee(prt->cost);

	int RefineChance = GetQuestFlag("main_quest_lv7.refine_chance");

	if (RefineChance > 0)
	{
		if (!item->CheckItemUseLevel(20) || item->GetType() != ITEM_WEAPON)
		{
			ChatPacket(CHAT_TYPE_INFO, LC_TEXT("ą«·á °ł·® ±âČ¸´Â 20 ŔĚÇĎŔÇ ą«±â¸¸ °ˇ´ÉÇŐ´Ď´Ů"));
			return false;
		}

		cost = 0;
		SetQuestFlag("main_quest_lv7.refine_chance", RefineChance - 1);
	}
	// END_OF_REFINE_COST

	if (result_vnum == 0)
	{
		ChatPacket(CHAT_TYPE_INFO, LC_TEXT("´ő ŔĚ»ó °ł·®ÇŇ Ľö ľř˝Ŕ´Ď´Ů."));
		return false;
	}

	if (item->GetType() == ITEM_USE && item->GetSubType() == USE_TUNING)
		return false;

	TItemTable * pProto = ITEM_MANAGER::instance().GetTable(item->GetRefinedVnum());

	if (!pProto)
	{
		sys_err("DoRefine NOT GET ITEM PROTO %d", item->GetRefinedVnum());
		ChatPacket(CHAT_TYPE_INFO, LC_TEXT("ŔĚ ľĆŔĚĹŰŔş °ł·®ÇŇ Ľö ľř˝Ŕ´Ď´Ů."));
		return false;
	}

	// Check level limit in korea only
	if (!g_iUseLocale)
	{
		for (int i = 0; i < ITEM_LIMIT_MAX_NUM; ++i)
		{
			long limit = pProto->aLimits[i].lValue;

			switch (pProto->aLimits[i].bType)
			{
			case LIMIT_LEVEL:
				if (GetLevel() < limit)
				{
					ChatPacket(CHAT_TYPE_INFO, LC_TEXT("°ł·®µČ ČÄ ľĆŔĚĹŰŔÇ ·ąş§ Á¦ÇŃş¸´Ů ·ąş§ŔĚ ł·˝Ŕ´Ď´Ů."));
					return false;
				}
				break;
			}
		}
	}

	// REFINE_COST
	if (GetGold() < cost)
	{
		ChatPacket(CHAT_TYPE_INFO, LC_TEXT("°ł·®Ŕ» ÇĎ±â Ŕ§ÇŃ µ·ŔĚ şÎÁ·ÇŐ´Ď´Ů."));
		return false;
	}

	if (!bMoneyOnly && !RefineChance)
	{
		for (int i = 0; i < prt->material_count; ++i)
		{
			if (CountSpecifyItem(prt->materials[i].vnum) < prt->materials[i].count)
			{
				if (test_server)
				{
					ChatPacket(CHAT_TYPE_INFO, "Find %d, count %d, require %d", prt->materials[i].vnum, CountSpecifyItem(prt->materials[i].vnum), prt->materials[i].count);
				}
				ChatPacket(CHAT_TYPE_INFO, LC_TEXT("°ł·®Ŕ» ÇĎ±â Ŕ§ÇŃ Ŕç·á°ˇ şÎÁ·ÇŐ´Ď´Ů."));
				return false;
			}
		}

		for (int i = 0; i < prt->material_count; ++i)
			RemoveSpecifyItem(prt->materials[i].vnum, prt->materials[i].count);
	}

	int prob = number(1, 100);

	if (IsRefineThroughGuild() || bMoneyOnly)
		prob -= 10;

	// END_OF_REFINE_COST

	int refineProb = prt->prob;
	unsigned char cancer = 0;
	if (refinePotions)
		for (size_t i = 0; i < 3; i++)
			if (refinePotions[i])
			{
				cancer += (i + 1) * 5;
				refinePotions[i]->SetCount(refinePotions[i]->GetCount() - 1);
			}

	if (prob <= MINMAX(refineProb, refineProb + cancer, 100))
	{
		// Ľş°ř! ¸đµç ľĆŔĚĹŰŔĚ »ç¶óÁö°í, °°Ŕş ĽÓĽşŔÇ ´Ů¸Ą ľĆŔĚĹŰ Čąµć
		LPITEM pkNewItem = ITEM_MANAGER::instance().CreateItem(result_vnum, 1, 0, false);

		if (pkNewItem)
		{
			ITEM_MANAGER::CopyAllAttrTo(item, pkNewItem);
			LogManager::instance().ItemLog(this, pkNewItem, "REFINE SUCCESS", pkNewItem->GetName());

			BYTE bCell = item->GetCell();

			// DETAIL_REFINE_LOG
			NotifyRefineSuccess(this, item, "REFINE_TYPE_ENCHANTED_FOREST");
			DBManager::instance().SendMoneyLog(MONEY_LOG_REFINE, item->GetVnum(), -cost);
			ITEM_MANAGER::instance().RemoveItem(item, "REMOVE (REFINE SUCCESS)");
			// END_OF_DETAIL_REFINE_LOG

			pkNewItem->AddToCharacter(this, TItemPos(INVENTORY, bCell));
			ITEM_MANAGER::instance().FlushDelayedSave(pkNewItem);

			sys_log(0, "Refine Success %d", cost);
			pkNewItem->AttrLog();
			//PointChange(POINT_GOLD, -cost);
			sys_log(0, "PayPee %d", cost);
			PayRefineFee(cost);
			sys_log(0, "PayPee End %d", cost);
		}

		//LIL_TARD_BUGO
		else if (result_fail_vnum && bMoneyOnly)
		{
			LPITEM pkNewItem = ITEM_MANAGER::instance().CreateItem(result_fail_vnum, 1, 0, false);

			if (pkNewItem)
			{
				ITEM_MANAGER::CopyAllAttrTo(item, pkNewItem);
				LogManager::instance().ItemLog(this, pkNewItem, "REFINE FAIL", pkNewItem->GetName());

				BYTE bCell = item->GetCell();

				DBManager::instance().SendMoneyLog(MONEY_LOG_REFINE, item->GetVnum(), -cost);
				NotifyRefineFail(this, item, "REFINE_TYPE_ENCHANTED_FOREST");
				ITEM_MANAGER::instance().RemoveItem(item, "REMOVE (REFINE FAIL)");

				pkNewItem->AddToCharacter(this, TItemPos(INVENTORY, bCell));

				ITEM_MANAGER::instance().FlushDelayedSave(pkNewItem);

				pkNewItem->AttrLog();
				PayRefineFee(cost);
			}
			else
			{
				sys_err("Cannot create item %u", result_fail_vnum);
				NotifyRefineFail(this, item, "REFINE_TYPE_ENCHANTED_FOREST");
			}

		}
		else
		{
			// DETAIL_REFINE_LOG
			// ľĆŔĚĹŰ »ýĽşżˇ ˝ÇĆĐ -> °ł·® ˝ÇĆĐ·Î °ŁÁÖ
			sys_err("cannot create item %u", result_vnum);
			NotifyRefineFail(this, item, IsRefineThroughGuild() ? "GUILD" : "POWER");
			// END_OF_DETAIL_REFINE_LOG
		}
	}
	else
	{
		// ˝ÇĆĐ! ¸đµç ľĆŔĚĹŰŔĚ »ç¶óÁü.
		DBManager::instance().SendMoneyLog(MONEY_LOG_REFINE, item->GetVnum(), -cost);
		NotifyRefineFail(this, item, IsRefineThroughGuild() ? "GUILD" : "POWER");
		item->AttrLog();
		ITEM_MANAGER::instance().RemoveItem(item, "REMOVE (REFINE FAIL)");

		//PointChange(POINT_GOLD, -cost);
		PayRefineFee(cost);
	}

	return true;
}