bool inv_sort(LPITEM item1, LPITEM item2) { return item1->GetVnum() < item2->GetVnum(); }


bool inventory_stack(LPCHARACTER ch, LPITEM item)
{
	std::vector<LPITEM> CollectItems;

	for (WORD i = 0; i < INVENTORY_MAX_NUM; i++)
	{
		LPITEM item2 = ch->GetInventoryItem(i);

		if (item2)
			CollectItems.push_back(item2);
	}

	for (std::vector<LPITEM>::iterator it = CollectItems.begin(); it != CollectItems.end(); it++)
	{
		LPITEM& item2 = *it;
		
		if (item2)
		{
			if (item->IsStackable() && item != item2 && !IS_SET(item->GetAntiFlag(), ITEM_ANTIFLAG_STACK) && item->GetVnum() == item2->GetVnum())
			{
				ch->MoveItem(TItemPos(INVENTORY, item2->GetCell()), TItemPos(INVENTORY, item->GetCell()), 0);
			}
		}
	}

	return true;
}

ACMD(do_sort_inventory)
{
	for (WORD i = 0; i < INVENTORY_MAX_NUM; ++i) {
		LPITEM item = ch->GetInventoryItem(i);

		if (item)
			inventory_stack(ch, item);
	}

	std::vector<LPITEM> collectItems;
	std::vector<WORD> oldCells;
	int totalSize = 0;
	for (WORD i = 0; i < INVENTORY_MAX_NUM; ++i) {
		LPITEM item = ch->GetInventoryItem(i);
		if (item) {
			totalSize += item->GetSize();
			oldCells.push_back(item->GetCell());
			collectItems.push_back(item);
		}
	}
	if (totalSize - 3 >= INVENTORY_MAX_NUM) {
		ch->ChatPacket(CHAT_TYPE_INFO, LC_TEXT("INVENTORY_FULL_CANNOT_SORT"));
		return;
	}

	for (std::vector<LPITEM>::iterator it = collectItems.begin(); it != collectItems.end(); ++it)
	{
		LPITEM& item = *it;
		//quickslot fix
		for (int i = 0; i <= QUICKSLOT_MAX_NUM; i++)
		{
			TQuickslot *pSlot;
			if (ch->GetQuickslot(i, &pSlot) && pSlot->type == QUICKSLOT_TYPE_ITEM)
				ch->DelQuickslot(i);
		}

		item->RemoveFromCharacter();
	}



	std::sort(collectItems.begin(), collectItems.end(), inv_sort);

	for (std::vector<LPITEM>::iterator it = collectItems.begin(); it != collectItems.end(); ++it)
	{
		LPITEM& sortedItem = *it;
		int cell = ch->GetEmptyInventory(sortedItem->GetSize());
		sortedItem->AddToCharacter(ch, TItemPos(INVENTORY, cell));
	}
}